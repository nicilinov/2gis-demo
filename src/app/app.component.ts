import { Component, AfterViewInit } from '@angular/core';
import DG from '2gis-maps'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements AfterViewInit {
  title = 'double-gis-demo';

  lat = 54.98;
  lng = 82.89;

  map = null

  get pos(): [number, number] {
    return [this.lat, this.lng]
  }

  ngAfterViewInit() {
    this.map = DG.map('map', {
      'center': [this.lat, this.lng],
      'zoom': 5,
      fullscreenControl: false,
      zoomControl: false
    });

    this.map.locate({ setView: true, watch: true })
      .on('locationfound', (e) => {
        //DG.marker([e.latitude, e.longitude]).addTo(this.map);
        this.lat = e.latitude
        this.lng = e.longitude

        this.showMePosition(this.pos)
      })
      .on('locationerror', (e) => {
        DG.popup()
          .setLatLng(this.map.getCenter())
          .setContent('Доступ к определению местоположения отключён')
          .openOn(this.map);
      });

  }

  showMePosition(pos: [number, number]) {
    var wrapIcon = DG.divIcon({ className: 'wrap-icon' });
    DG.marker(pos, { icon: wrapIcon }).addTo(this.map);

    var icon = DG.divIcon({ className: 'icon' });
    DG.marker(pos, { icon }).addTo(this.map);


  }
}
